-- Setting leader key
vim.g.mapleader = " "

-- Open netrw
vim.keymap.set("n", "<leader>pe", vim.cmd.Ex)

vim.keymap.set("n", "<leader>fm", vim.lsp.buf.format)
